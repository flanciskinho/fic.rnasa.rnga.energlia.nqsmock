# NQS nock

This proyect it's to have typical commands for SGE systems and it allows us to make some test

## Configuration

### Database

Create the following environment variable to have access for SQLite

	NQSMOCK_DB_URL
	
To create tables and insert some data move to _sql_ directory and execute `sh create.sh`

### Files

Configure the following environment variable with a path to store all output files

	NQSMOCK_JOB_DIR

## Install

The first step is compile

	cd src
	sh compile.sh
	
The next step is move files to some directory include on PATH for example `/usr/local/bin`

	mv qsub  /usr/local/bin
	mv qdel  /usr/local/bin
	mv qstat /usr/local/bin
	mv qdebug /usr/local/bin
