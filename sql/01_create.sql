CREATE TABLE ssh_server (
	id INTEGER PRIMARY KEY  AUTOINCREMENT,
	dnsname VARCHAR(255) NOT NULL UNIQUE,
	max_job BIGINT NOT NULL,
	max_mem BIGINT NOT NULL,
	max_proc BIGINT NOT NULL,
	max_time BIGINT NOT NULL
);

CREATE TABLE ssh_account (
	id INTEGER PRIMARY KEY  AUTOINCREMENT,
	username VARCHAR(255) NOT NULL,
	num_job INTEGER NOT NULL,
	belong BIGINT NOT NULL REFERENCES ssh_server (id)
);

CREATE TABLE job (
	job_id INTEGER PRIMARY KEY  AUTOINCREMENT,
	status CHAR(1) NOT NULL,
	timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	num_proc INTEGER NOT NULL,
	memory BIGINT NOT NULL,
	scriptname VARCHAR(255) NOT NULL,
	launch_by BIGINT NOT NULL REFERENCES ssh_account (id)
);
