INSERT INTO
	ssh_server (id, dnsname, max_job, max_mem, max_proc, max_time)
VALUES
	(100, 'svg.cesga.es', 5, 2*1024*1024*1024, 4, 60),
	(101, 'ft2.cesga.es', 10, 8*1024*1024*1024, 16, 3600);

INSERT INTO
	ssh_account(id, username, num_job, belong)
VALUES
	(100, 'ulctifcs', 0, 100),
	(101, 'ulctinvb', 0, 100),
	(102, 'ulctiefb', 0, 100),

	(103, 'ft2user1', 0, 101),
	(104, 'ft2user2', 0, 101),
	(105, 'ft2user3', 0, 101),
	(106, 'ft2user4', 0, 101),
	(107, 'ft2user5', 0, 101);

INSERT INTO
	job(status, timestamp, num_proc, memory, scriptname, launch_by)
VALUES
	('r', CURRENT_TIMESTAMP, 1, 111, '1job100.sh', 100),
	('r', CURRENT_TIMESTAMP, 2, 122, '2job100.sh', 100),
	('r', CURRENT_TIMESTAMP, 3, 133, '3job100.sh', 100),
	('w', CURRENT_TIMESTAMP, 4, 144, '4job100.sh', 100),
	('w', CURRENT_TIMESTAMP, 5, 155, '5job100.sh', 100),

	('r', CURRENT_TIMESTAMP, 1, 211, '1job101.sh', 101),
	('r', CURRENT_TIMESTAMP, 2, 222, '2job101.sh', 101),
	('w', CURRENT_TIMESTAMP, 3, 233, '3job101.sh', 101);