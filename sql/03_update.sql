UPDATE ssh_account
SET num_job =
	(SELECT count(j.job_id)
	 FROM job j
	 WHERE j.launch_by = id);
