#!/bin/bash

CC=clang

FLAGS="-lm -Wall -l sqlite3 -v -O3"

declare -a TEST_DIR=("domain" "util")
declare -a PROG=("qsub" "qdel" "qstat" "qdebug")


for ((i = 0; i < ${PROG[@]}; i++))
do
	rm ${PROG[${i}]}
done

for ((i = 0; i < ${#TEST_DIR[@]}; i++))
do
	${CC} -c  `find ${TEST_DIR[${i}]} -name "*.c"`
done

TMP=""
for fileo in `ls *.o`
do
        TMP="$TMP $fileo"
done

for ((i = 0; i < ${#PROG[@]}; i++))
do
        ${CC} ${PROG[${i}]}.c ${TMP} ${FLAGS} -o ${PROG[${i}]}
done

rm `find . -name "*.o"`
