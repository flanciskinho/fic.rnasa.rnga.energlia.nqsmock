#include "Datasource.h"
#include "../util/UtilFunctions.h"

static char QUERY_LAST_ROWID[] = "select last_insert_rowid()";

char *getUrlDatasource() {
	char *tmp = getenv("NQSMOCK_DB_URL");

	return tmp;
}

bool selectSqlStatement(sqlite3 *db, char *sql, /* SQL to be evaluated */ int (*callback)(void*,int,char**,char**)) {
   int rc;
   char *zErrMsg = 0;

   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

   if (rc != SQLITE_OK ) {
      printf("Failed to select data\nSQL error: %s", zErrMsg);
      sqlite3_free(zErrMsg);
      return exit_msg("selectSqlStatement", "SQL error");
   } 

   return true;
}

long getLastRowId(sqlite3 *db) {
	sqlite3_stmt *res;
	int rc;

	rc = sqlite3_prepare_v2(db, QUERY_LAST_ROWID, -1, &res, 0);
	if (rc != SQLITE_OK) {
		printf("Failed to fetch data: %s\n", sqlite3_errmsg(db));
		exit_msg("getLastRowId", "failed to execute sql");
		return -1l;
	}

	rc = sqlite3_step(res);
    
    if (rc == SQLITE_ROW)
    	return atol(sqlite3_column_text(res, 0));

	return -1l;
}

sqlite3 *openDatasource(char *url) {
	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;

	if (url == NULL)
		exit_msg("openDatasource", "invalid datasource url");

	rc = sqlite3_open(url, &db);

	if (rc) {
		printf("Can't open database: %s\n", sqlite3_errmsg(db));
		exit_msg("openDatasource", "Can't open database");
	}
	

#ifdef NQSMOCK_DEBUG
	printf("Opened database successfully\n");
#endif

	return db;
}

void closeDatasource(sqlite3 *db) {
	sqlite3_close(db);
#ifdef NQSMOCK_DEBUG
	printf("Closed database successfully\n");
#endif
}