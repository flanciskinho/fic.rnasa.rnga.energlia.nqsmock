#ifndef NQSMOCK_Datasource_h
#define NQSMOCK_Datasource_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <sqlite3.h>

char *getUrlDatasource();

sqlite3 *openDatasource(char *url);
void closeDatasource(sqlite3 *db);

bool selectSqlStatement(sqlite3 *db, char *sql, int (*callback)(void*,int,char**,char**));

long getLastRowId(sqlite3 *db);


#endif