#include "Job.h"
#include "Datasource.h"
#include "../util/UtilFunctions.h"

#include <string.h>

static char TABLE_NAME[] = "job";

static bool valid = false;
static Job tmpEntity;

static Job *dupEntity() {
   Job *aux = malloc(sizeof(Job));

   if (aux == NULL) {
      exit_msg("dupEntity", "cannot allocate memory");
      return NULL;
   }

   aux->jobId = tmpEntity.jobId ;
   aux->status = tmpEntity.status ;
   aux->timestamp = tmpEntity.timestamp ;
   aux->numProc = tmpEntity.numProc ;
   aux->memory = tmpEntity.memory ;
   aux->scriptname = tmpEntity.scriptname ;
   aux->launchBy = tmpEntity.launchBy ;

   return aux;
}

static int selectCallback(void *NotUsed, int argc, char **argv, char **azColName) {
   NotUsed = 0;

   for (int i = 0; i < argc; i++) {

#ifdef NQSMOCK_DEBUG
  printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
#endif
      if (!strcmp(azColName[i], "job_id")) {
         tmpEntity.jobId = (argv[i] ? (int) atol(argv[i]): -1);
      } else if (!strcmp(azColName[i], "status")) {
         tmpEntity.status = (argv[i] ? argv[i][0]: ' ');
      } else if (!strcmp(azColName[i], "timestamp")) {
         tmpEntity.timestamp = argv[i] ? strdup(argv[i]): NULL;
      } else if (!strcmp(azColName[i], "num_proc")) {
         tmpEntity.numProc = argv[i] ? (int) atol(argv[i]): -1;
      } else if (!strcmp(azColName[i], "memory")) {
         tmpEntity.memory = argv[i] ? atol(argv[i]): -1;
      } else if (!strcmp(azColName[i], "scriptname")) {
         tmpEntity.scriptname = argv[i] ? strdup(argv[i]): NULL;
      } else if (!strcmp(azColName[i], "launch_by")) {
         tmpEntity.launchBy = argv[i] ? (int) atol(argv[i]): -1;
      } else {
         printf("column %s don't match with entity\n", azColName[i]);
      }
   }
   valid = true;

   return 0;
}

Job *insertJob(sqlite3 *db, Job *job) {
	char *zErrMsg = 0;
	int rc;
	char *sql;

	char *sqlPattern = "INSERT INTO %s (status, timestamp, num_proc, memory, scriptname, launch_by)"
   "VALUES('%c', CURRENT_TIMESTAMP, %d, %ld, '%s', %d);";

   sql = malloc( (strlen(TABLE_NAME) + strlen(sqlPattern) +  strlen(job->scriptname) + 3*32 + 1) * sizeof(char) );
   if (sql == NULL) {
      exit_msg("insertJob", "cannot allocate memory");
      return NULL;
   }

   sprintf(sql, sqlPattern, TABLE_NAME, job->status, job->numProc, job->memory, job->scriptname, job->launchBy);

   /* Execute SQL statement */
   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   if( rc != SQLITE_OK ){
      printf("SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
      free(sql);
      exit_msg("insertJob", "cannot execute sql statement");
      return NULL;
   }

#ifdef NQSMOCK_DEBUG
   fprintf(stdout, "Record created successfully\n");
#endif

   free(sql);

   job->jobId = (int) getLastRowId(db);

   Job *tmp = getOneJobByJobId(db, job->jobId);
   job->timestamp = strdup(tmp->timestamp);

   free(tmp->timestamp);
   free(tmp->scriptname);
   free(tmp);

   return job;
}

Job *updateJob(sqlite3 *db, Job *job) {
	char *zErrMsg = 0;
	int rc;
	char *sql;

  char *sqlPattern = "UPDATE %s SET status = '%c', timestamp = CURRENT_TIMESTAMP, num_proc = '%d', memory = %ld, scriptname = '%s', launch_by = %d WHERE job_id = %d";

   sql = malloc( (strlen(TABLE_NAME) + strlen(sqlPattern) +  strlen(job->scriptname) + 3*32 + 1) * sizeof(char) );
   if (sql == NULL) {
      exit_msg("updateJob", "cannot allocate memory");
      return NULL;
   }

   sprintf(sql, sqlPattern, TABLE_NAME, job->status, job->numProc, job->memory, job->scriptname, job->launchBy, job->jobId);

   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   return NULL;
   if( rc != SQLITE_OK ){
      printf("SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
      free(sql);
      exit_msg("updateJob", "cannot execute sql statement");
      return NULL;
   }

#ifdef NQSMOCK_DEBUG
   fprintf(stdout, "Record updated successfully\n");
#endif

   free(sql);

   return job;
}

Job *getOneJobByJobId(sqlite3 *db, int id) {
   char *sqlPattern = "SELECT * FROM %s WHERE job_id = %d;";
   char *sql;

   sql = malloc( (strlen(TABLE_NAME) + strlen(sqlPattern) + 1*32) * sizeof(char) );
   if (sql == NULL) {
      exit_msg("getJobById", "cannot allocate memory");
      return NULL;
   }

   sprintf(sql, sqlPattern, TABLE_NAME, id);
   valid = false;
   if (!selectSqlStatement(db, sql, selectCallback))
      return NULL;

   return valid? dupEntity(): NULL;
}

Job *getOneJobByLaunchByAndIndex(sqlite3 *db, int launchBy, int index) {
   char *sqlPattern = "SELECT * FROM %s WHERE launch_by = %d ORDER BY job_id LIMIT %d,1;";
   char *sql;

   sql = malloc( (strlen(TABLE_NAME) + strlen(sqlPattern) + 2*32) * sizeof(char) );
   if (sql == NULL) {
      exit_msg("getSshAccountById", "cannot allocate memory");
      return NULL;
   }

   sprintf(sql, sqlPattern, TABLE_NAME, launchBy, index);

   valid = false;
   if (!selectSqlStatement(db, sql, selectCallback))
      return NULL;

   return valid? dupEntity(): NULL;
}

Job *getFirstJobByLaunchBy(sqlite3 *db, int launchBy) {
   return getOneJobByLaunchByAndIndex(db, launchBy, 0);
}

void deleteJobByJobId(sqlite3 *db, int jobId) {
   char *sqlPattern = "DELETE FROM %s WHERE job_id = %d;";
   char *sql;

   char *zErrMsg = 0;
   int rc;

   sql = malloc( (strlen(TABLE_NAME) + strlen(sqlPattern) + 1*32) * sizeof(char) );

   if (sql == NULL) {
      exit_msg("getJobById", "cannot allocate memory");
      return;
   }

   sprintf(sql, sqlPattern, TABLE_NAME, jobId);

   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   if( rc != SQLITE_OK ){
      printf("SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
      free(sql);
      exit_msg("deleteJobByJobId", "cannot execute sql statement");
   }
}

void toStringJob(Job *job) {
   if (job == NULL) {
      printf("Job = null;\n\n");
      return;
   }

   printf("Job = {\n");
   printf("\tjobId='%d',\n", job->jobId);
   printf("\tstatus='%c',\n", job->status);
   printf("\ttimestamp='%s',\n", job->timestamp);
   printf("\tnumProc='%d',\n", job->numProc);
   printf("\tmemory='%ld',\n", job->memory);
   printf("\tscriptname='%s,'\n", job->scriptname);
   printf("\tlaunchBy='%d'\n", job->launchBy);
   printf("}\n");
}
