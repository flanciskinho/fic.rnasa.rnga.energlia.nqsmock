#ifndef NQSMOCK_Job_h
#define NQSMOCK_Job_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <sqlite3.h>

typedef struct {
    int jobId;
    char status;
    char *timestamp;
    int numProc;
    long memory;
    char *scriptname;
    int launchBy;
} Job;

Job *insertJob(sqlite3 *db, Job *job);

Job *updateJob(sqlite3 *db, Job *job);

Job *getOneJobByJobId(sqlite3 *db, int id);
Job *getFirstJobByLaunchBy(sqlite3 *db, int launchBy);
Job *getOneJobByLaunchByAndIndex(sqlite3 *db, int launchBy, int index);

void deleteJobByJobId(sqlite3 *db, int jobId);

void toStringJob(Job *job);

#endif
