#include "SshAccount.h"
#include "Datasource.h"
#include "../util/UtilFunctions.h"

#include <string.h>

static char TABLE_NAME[] = "ssh_account";

static bool valid = false;
static SshAccount tmpEntity;

static SshAccount *dupEntity() {
   SshAccount *aux = malloc(sizeof(SshAccount));

   if (aux == NULL) {
      exit_msg("dupEntity", "cannot allocate memory");
      return NULL;
   }

   aux->id = tmpEntity.id ;
   aux->username = tmpEntity.username ;
   aux->numJob = tmpEntity.numJob ;
   aux->belong = tmpEntity.belong ;

   return aux;
}

static int selectCallback(void *NotUsed, int argc, char **argv, char **azColName) {
   NotUsed = 0;

   for (int i = 0; i < argc; i++) {

#ifdef NQSMOCK_DEBUG
  printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
#endif
      if (!strcmp(azColName[i], "id")) {
         tmpEntity.id = (argv[i] ? (int) atol(argv[i]): -1);
      } else if (!strcmp(azColName[i], "username")) {
         tmpEntity.username = (argv[i] ? strdup(argv[i]): NULL);
      } else if (!strcmp(azColName[i], "num_job")) {
         tmpEntity.numJob = argv[i] ? (int) atol(argv[i]): -1;
      } else if (!strcmp(azColName[i], "belong")) {
         tmpEntity.belong = argv[i] ? (int) atol(argv[i]): -1;
      } else {
         printf("column %s don't match with entity\n", azColName[i]);
      }
   }    
   valid = true;

   return 0;
}

SshAccount *insertSshAccount(sqlite3 *db, SshAccount *sshAccount) {
	char *zErrMsg = 0;
	int rc;
	char *sql;

	char *sqlPattern = "INSERT INTO %s (username, num_job, belong) VALUES('%s', %d, %d);";

   sql = malloc( (strlen(TABLE_NAME) + strlen(sqlPattern) +  strlen(sshAccount->username) + 2*32) * sizeof(char) );
   if (sql == NULL) {
      exit_msg("insertSshAccount", "cannot allocate memory");
      return NULL;
   }

   sprintf(sql, sqlPattern, TABLE_NAME, sshAccount->username, sshAccount->numJob, sshAccount->belong);

   /* Execute SQL statement */
   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   if( rc != SQLITE_OK ){
      printf("SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
      free(sql);
      exit_msg("insertSshAccount", "cannot execute sql statement");
      return NULL;
   }

#ifdef NQSMOCK_DEBUG
   fprintf(stdout, "Record created successfully\n");
#endif

   free(sql);

   sshAccount->id = (int) getLastRowId(db);

   return sshAccount;
}

SshAccount *getOneSshAccountById(sqlite3 *db, int id) {
   char *sqlPattern = "SELECT * FROM %s WHERE id = %d;";
   char *sql;

   sql = malloc( (strlen(TABLE_NAME) + strlen(sqlPattern) + 1*32) * sizeof(char) );
   if (sql == NULL) {
      exit_msg("getSshAccountById", "cannot allocate memory");
      return NULL;
   }

   sprintf(sql, sqlPattern, TABLE_NAME, id);

   valid = false;
   if (!selectSqlStatement(db, sql, selectCallback))
      return NULL;

   return valid? dupEntity(): NULL;
}

SshAccount *getFirstSshAccountByUsername(sqlite3 *db, char *username) {
   char *sqlPattern = "SELECT * FROM %s WHERE username LIKE '%s' LIMIT 1;";
   char *sql;

   sql = malloc( (strlen(TABLE_NAME) + strlen(sqlPattern) + strlen(username)) * sizeof(char) );
   if (sql == NULL) {
      exit_msg("getSshAccountById", "cannot allocate memory");
      return NULL;
   }

   sprintf(sql, sqlPattern, TABLE_NAME, username);

   valid = false;
   if (!selectSqlStatement(db, sql, selectCallback))
      return NULL;

   return valid? dupEntity(): NULL;
}


static void sshAccountAddSubNumJob(sqlite3 *db, int id, bool add) {
   char *zErrMsg = 0;
   int rc;

   char *sqlPattern = "UPDATE %s SET num_job = num_job %c 1 WHERE id = %d;";
   char *sql;

   sql = malloc( (strlen(sqlPattern) + strlen(TABLE_NAME) + 32 + 1 ) * sizeof(char)  );
   if (sql == NULL) {
      exit_msg("sshAccountAddNumJob", "cannot allocate memory");
   }
   sprintf(sql, sqlPattern, TABLE_NAME, (add? '+': '-'), id);

   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   if( rc != SQLITE_OK ){
      printf("SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
      exit_msg("sshAccountAddNumJob", "SQL error");
   }
}

void sshAccountAddNumJob(sqlite3 *db, int id) {
   sshAccountAddSubNumJob(db, id, true);
}

void sshAccountSubNumJob(sqlite3 *db, int id) {
   sshAccountAddSubNumJob(db, id, false);
}

void toStringSshAccount(SshAccount *sshAccount) {
   if (sshAccount == NULL) {
      printf("SshAccount = null;\n\n");
      return;
   }

   printf("SshAccount = {\n");
   printf("\tid='%d',\n", sshAccount->id);
   printf("\tusername='%s',\n", (sshAccount->username == NULL? "null": sshAccount->username));
   printf("\tnumJob='%d',\n", sshAccount->numJob);
   printf("\tbelong='%d'\n", sshAccount->belong);
   printf("}\n");
}

   