#ifndef NQSMOCK_SshAccount_h
#define NQSMOCK_SshAccount_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <sqlite3.h>

typedef struct {
    int id;
    char *username;
    int numJob;
    int belong;
} SshAccount;


SshAccount *insertSshAccount(sqlite3 *db, SshAccount *sshAccount);

SshAccount *getOneSshAccountById(sqlite3 *db, int id);
SshAccount *getFirstSshAccountByUsername(sqlite3 *db, char *username);

void sshAccountAddNumJob(sqlite3 *db, int id);
void sshAccountSubNumJob(sqlite3 *db, int id);

void toStringSshAccount(SshAccount *sshAccount);

#endif