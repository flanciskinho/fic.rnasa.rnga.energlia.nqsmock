#include "SshServer.h"
#include "Datasource.h"
#include "../util/UtilFunctions.h"

#include <string.h>

static char TABLE_NAME[] = "ssh_server";

static bool valid = false;
static SshServer tmpEntity;

static SshServer *dupEntity() {
   SshServer *aux = malloc(sizeof(SshServer));

   if (aux == NULL) {
      exit_msg("dupEntity", "cannot allocate memory");
      return NULL;
   }

   aux->id = tmpEntity.id ;
   aux->dnsname = strdup(tmpEntity.dnsname) ;
   aux->maxJob = tmpEntity.maxJob ;
   aux->maxMem = tmpEntity.maxMem ;
   aux->maxProc = tmpEntity.maxProc ;
   aux->maxTime = tmpEntity.maxTime ;

   return aux;
}

static int selectCallback(void *NotUsed, int argc, char **argv, char **azColName) {
   NotUsed = 0;
   for (int i = 0; i < argc; i++) {

#ifdef NQSMOCK_DEBUG
  printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
#endif
      if (!strcmp(azColName[i], "id")) {
         tmpEntity.id = (argv[i] ? (int) atol(argv[i]): -1);
      } else if (!strcmp(azColName[i], "dnsname")) {
         tmpEntity.dnsname = (argv[i] ? strdup(argv[i]): NULL);
      } else if (!strcmp(azColName[i], "max_job")) {
         tmpEntity.maxJob = argv[i] ? atol(argv[i]): -1;
      } else if (!strcmp(azColName[i], "max_mem")) {
         tmpEntity.maxMem = argv[i] ? atol(argv[i]): -1;
      } else if (!strcmp(azColName[i], "max_proc")) {
         tmpEntity.maxProc = argv[i] ? atol(argv[i]): -1;
      } else if (!strcmp(azColName[i], "max_time")) {
         tmpEntity.maxTime = argv[i] ? atol(argv[i]): -1;
      } else {
         printf("column %s don't match with entity\n", azColName[i]);
      }
   }  
    
   valid = true;
   return 0;
}

SshServer *insertSshServer(sqlite3 *db, SshServer *sshServer) {
	char *zErrMsg = 0;
	int rc;
	char *sql;

	char *sqlPattern = "INSERT INTO %s (dnsname, max_job, max_mem, max_proc, max_time) VALUES('%s', %ld, %ld, %ld, %ld);";

   sql = malloc( (strlen(TABLE_NAME) + strlen(sqlPattern) +  strlen(sshServer->dnsname) + 4*32) * sizeof(char) );
   if (sql == NULL) {
      exit_msg("insertSshServer", "cannot allocate memory");
      return NULL;
   }

   sprintf(sql, sqlPattern, TABLE_NAME, sshServer->dnsname, sshServer->maxJob, sshServer->maxMem, sshServer->maxProc, sshServer->maxTime);

   /* Execute SQL statement */
   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   if( rc != SQLITE_OK ){
      printf("SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
      free(sql);
      exit_msg("insertSshServer", "cannot execute sql statement");
      return NULL;
   }

#ifdef NQSMOCK_DEBUG
   fprintf(stdout, "Record created successfully\n");
#endif

   free(sql);

   sshServer->id = (int) getLastRowId(db);

   return sshServer;
}

SshServer *getOneSshServerById(sqlite3 *db, int id) {
   char *sqlPattern = "SELECT * FROM %s WHERE id = %d;";
   char *sql;

   sql = malloc( (strlen(TABLE_NAME) + strlen(sqlPattern) + 1*32) * sizeof(char) );
   if (sql == NULL) {
      exit_msg("getSshServerById", "cannot allocate memory");
      return NULL;
   }

   sprintf(sql, sqlPattern, TABLE_NAME, id);

   valid = false;
   if (!selectSqlStatement(db, sql, selectCallback))
      return NULL;

   return valid? dupEntity(): NULL;
}

SshServer *getFirstSshServerByDnsname(sqlite3 *db, char *dnsname) {
   char *sqlPattern = "SELECT * FROM %s WHERE dnsname LIKE '%s' LIMIT 1;";
   char *sql;

   sql = malloc( (strlen(TABLE_NAME) + strlen(sqlPattern) + strlen(dnsname)) * sizeof(char) );
   if (sql == NULL) {
      exit_msg("getSshServerById", "cannot allocate memory");
      return NULL;
   }

   sprintf(sql, sqlPattern, TABLE_NAME, dnsname);

   valid = false;
   if (!selectSqlStatement(db, sql, selectCallback))
      return NULL;

   return valid? dupEntity(): NULL;
}



void toStringSshServer(SshServer *sshServer) {
   if (sshServer == NULL) {
      printf("SshServer = null;\n");
      return;
   }

   printf("SshServer = {\n");
   printf("\tid='%d',\n", sshServer->id);
   printf("\tdnsname='%s',\n", (sshServer->dnsname == NULL? "null": sshServer->dnsname));
   printf("\tmaxJob='%ld',\n", sshServer->maxJob);
   printf("\tmaxProc='%ld',\n", sshServer->maxProc);
   printf("\tmaxMem='%ld',\n", sshServer->maxMem);
   printf("\tmaxTime='%ld'\n", sshServer->maxTime);
   printf("}\n");
}

   