#ifndef NQSMOCK_SshServer_h
#define NQSMOCK_SshServer_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <sqlite3.h>

typedef struct {
    int id;
    char *dnsname;
    long maxJob;
    long maxMem;
    long maxProc;
    long maxTime;
} SshServer;

SshServer *insertSshServer(sqlite3 *db, SshServer *sshServer);

SshServer *getOneSshServerById(sqlite3 *db, int id);
SshServer *getFirstSshServerByDnsname(sqlite3 *db, char *dnsname);

void toStringSshServer(SshServer *sshServer);

#endif