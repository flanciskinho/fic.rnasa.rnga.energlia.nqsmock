#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <sqlite3.h>

#include "domain/Datasource.h"
#include "domain/SshServer.h"
#include "domain/SshAccount.h"
#include "domain/Job.h"

int main() {
	sqlite3 *db = openDatasource(getUrlDatasource());

	SshServer sshServer;
	sshServer.dnsname = "svg.cesga.es";
	sshServer.maxJob = 10;
	sshServer.maxMem = 100;
	sshServer.maxProc = 5;
	sshServer.maxTime = 1000;
	insertSshServer(db, &sshServer);

	toStringSshServer(getOneSshServerById(db, sshServer.id));
	toStringSshServer(getFirstSshServerByDnsname(db, sshServer.dnsname));

	toStringSshServer(getOneSshServerById(db, -1));
	toStringSshServer(getFirstSshServerByDnsname(db, ""));

	SshAccount sshAccount;
	sshAccount.username = "ulctifcs";
	sshAccount.numJob = 0;
	sshAccount.belong = sshServer.id;

	insertSshAccount(db, &sshAccount);

	toStringSshAccount(getOneSshAccountById(db, sshAccount.id));
	toStringSshAccount(getFirstSshAccountByUsername(db, sshAccount.username));

	toStringSshAccount(getOneSshAccountById(db, -1));
	toStringSshAccount(getFirstSshAccountByUsername(db, ""));

	Job job;
	job.status = 'r';
	job.numProc = 1;
	job.memory = 2;
	job.scriptname = "job.sh";
	job.launchBy = sshAccount.id;

	insertJob(db, &job);
	toStringJob(getOneJobByJobId(db, job.jobId));

	toStringJob(getOneJobByJobId(db, -1));

	closeDatasource(db);
}
