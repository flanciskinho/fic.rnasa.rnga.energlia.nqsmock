#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>

#include <sqlite3.h>

#include "domain/Datasource.h"
#include "domain/SshServer.h"
#include "domain/SshAccount.h"
#include "domain/Job.h"

#include "util/UtilFunctions.h"

char *getCurrentUser() {
	return getenv("USER");
}

char *getDirectory() {
	return getenv("NQSMOCK_JOB_DIR");
}

void usage() {
	printf("qdebug jobid status\n");
	exit(EXIT_FAILURE);
}

void qdebug(sqlite3 *db, int jobId, char status) {
	Job *job = getOneJobByJobId(db, jobId);
	if (job == NULL) {
		printf("denied: job \"%d\" does not exist\n", jobId);
		return;
	}

  job->status = status;
  updateJob(db, job);
}


int main(int argc, char *argv[]) {
	if (argc != 3)
		usage();

	sqlite3 *db = openDatasource(getUrlDatasource());

  qdebug(db, (int) atol(argv[1]), argv[2][0]);

	closeDatasource(db);
}
