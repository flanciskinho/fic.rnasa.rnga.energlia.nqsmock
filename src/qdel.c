#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>

#include <sqlite3.h>

#include "domain/Datasource.h"
#include "domain/SshServer.h"
#include "domain/SshAccount.h"
#include "domain/Job.h"

#include "util/UtilFunctions.h"

char *getCurrentUser() {
	return getenv("USER");
}

char *getDirectory() {
	return getenv("NQSMOCK_JOB_DIR");
}

void usage() {
	printf("qdel jobid\nqdel -u username\n");
	exit(EXIT_FAILURE);
}

void qdelJob(sqlite3 *db, int jobId) {
	SshAccount *sshAccount = getFirstSshAccountByUsername(db, getCurrentUser());
	if (sshAccount == NULL) {
		printf("Don't exist user %s on database\n", getCurrentUser());
		exit_msg("main", "username invalid");
	}

	Job *job = getOneJobByJobId(db, jobId);
	if (job == NULL) {
		printf("denied: job \"%d\" does not exist\n", jobId);
		return;
	}

	if (job->launchBy != sshAccount->id) {
		printf("No tienes permisos para borralo\n");
		exit(EXIT_FAILURE);
	}

	deleteJobByJobId(db, job->jobId);
	sshAccountSubNumJob(db, sshAccount->id);

	printf("%s has registered the job %d for deletion\n", getCurrentUser(), jobId);

	char *pathname = malloc(sizeof(char) * (strlen(getDirectory()) + strlen(job->scriptname) + 32 + 3) );
	if (pathname == NULL) {
		exit_msg("qdelJob", "cannot allocate memory");
	}

	sprintf(pathname, "%s/%s.o%d", getDirectory(), job->scriptname, job->jobId);
	FILE *f = fopen(pathname, "w");
	if (f != NULL) {
		fclose(f);
	}

	sprintf(pathname, "%s/%s.e%d", getDirectory(), job->scriptname, job->jobId);
	f = fopen(pathname, "w");
	if (f != NULL) {
		fclose(f);
	}

}

void qdelUserJobs(sqlite3 *db, char *argv) {
	if (strcmp(getCurrentUser(), argv)) {
		printf("No tienes permisos para borralo\n");
		exit(EXIT_FAILURE);
	}

	SshAccount *sshAccount = getFirstSshAccountByUsername(db, getCurrentUser());
	if (sshAccount == NULL) {
		printf("Don't exist user %s on database\n", getCurrentUser());
		exit_msg("main", "username invalid");
	}

	Job *job = getFirstJobByLaunchBy(db, sshAccount->id);

	if (job == NULL) {
		printf("There is no job registered for the following users: %s\n", getCurrentUser());
		exit(0);
	}

	while (job != NULL) {
		qdelJob(db, job->jobId);
		job = getFirstJobByLaunchBy(db, sshAccount->id);
	}

}

int main(int argc, char *argv[]) {
	if (argc != 2 && argc != 3)
		usage();

	sqlite3 *db = openDatasource(getUrlDatasource());

	if (argc == 2) {
		qdelJob(db, (int) atol(argv[1]));
	}

	if (argc == 3) {
		if (strcmp(argv[1], "-u"))
			usage();

		qdelUserJobs(db, argv[2]);
	}

	closeDatasource(db);
}