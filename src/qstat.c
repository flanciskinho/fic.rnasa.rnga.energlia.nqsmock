#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>

#include <sqlite3.h>

#include "domain/Datasource.h"
#include "domain/SshServer.h"
#include "domain/SshAccount.h"
#include "domain/Job.h"

#include "util/UtilFunctions.h"

char *getCurrentUser() {
	return getenv("USER");
}

char *getDirectory() {
	return getenv("NQSMOCK_JOB_DIR");
}

void usage() {
	printf("qstat\n");
	exit(EXIT_FAILURE);
}

void showStatusHeader() {
	printf("job-ID\tprior\tname\tuser\tstate\tsubmit/start at\tqueue\tslots\tja-task-ID\n");
	printf("----------------------------------------------------------------------------------\n");
}

void showStatus(sqlite3 *db, Job *job) {
	printf("%d\t", job->jobId);
	printf("8.536\t");
	printf("%s\t", job->scriptname);
	printf("%s\t", getCurrentUser());
	printf("%c\t", job->status);
	printf("%s\t", job->timestamp);
	printf("g0_membig@compute-4.2.local\t");
	printf("%d\n", job->numProc);
}

void qstat(sqlite3 *db) {
	SshAccount *sshAccount = getFirstSshAccountByUsername(db, getCurrentUser());
	if (sshAccount == NULL) {
		printf("Don't exist user %s on database\n", getCurrentUser());
		exit_msg("main", "username invalid");
	}

	Job *job = getFirstJobByLaunchBy(db, sshAccount->id);

	if (job == NULL) {
		return;
	}

	showStatusHeader();

	int cnt = 0;
	while (job != NULL) {
		showStatus(db, job);
		cnt++;
		job = getOneJobByLaunchByAndIndex(db, sshAccount->id, cnt);
	}
}

int main(int argc, char *argv[]) {
	if (argc != 1)
		usage();

	sqlite3 *db = openDatasource(getUrlDatasource());

	qstat(db);

	closeDatasource(db);
}