#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>

#include <sqlite3.h>

#include "domain/Datasource.h"
#include "domain/SshServer.h"
#include "domain/SshAccount.h"
#include "domain/Job.h"

#include "util/UtilFunctions.h"

typedef struct {
    int  numProc;
	long time;
	long memory;
	long space;
	char *scriptname;
} QsubParam;



char *getCurrentUser() {
	return getenv("USER");
}

void usage() {
	printf("qsub -l num_proc=${NUM_PROC},s_rt=${TIME},s_vmem=2G,h_fsize=2G,arch=amd -cwd ${SCRIPT}\n");
	exit(EXIT_FAILURE);
}

long toBytes(char *value) {
	long tmp = atol(value);
	switch (value[strlen(value)-1]) {
		case 'G':
		case 'g':
			tmp *= 1024;
		case 'M':
		case 'm':
			tmp *= 1024;
		case 'K':
		case 'k':
			tmp *= 1024;
	}
	return tmp;
}

void setValues(QsubParam *qsubParam, char *param, char *value) {
	if (!strcmp(param, "num_proc")) {
		qsubParam->numProc = (int) atol(value);
		return;
	}

	if (!strcmp(param, "s_vmem")) {
		qsubParam->memory = toBytes(value);
		return;
	}

	if (!strcmp(param, "h_fsize")) {
		qsubParam->space = toBytes(value);
		return;
	}

	if (!strcmp(param, "s_rt")) {
		qsubParam->time = 0l;
		char *tok = value, *end = value;
		
		while (tok != NULL) {
			strsep(&end, ":");

			qsubParam->time = qsubParam->time*60l + atol(tok);

			tok = end;
		}

	}
}

QsubParam checkArgv(int argc, char *argv[]) {
	if (argc != 5)
		usage();

	if (strcmp(argv[0], "qsub") && strcmp(argv[0], "./qsub") )
		usage();

	if (strcmp(argv[1], "-l"))
		usage();

	if (strcmp(argv[3], "-cwd"))
		usage();

	QsubParam qsubParam;

	char *tok = argv[2], *end = argv[2];
	char *value;
	while (tok != NULL) {
		strsep(&end, ",");

		value = index(tok, '=');
		*value = '\0';
		value++;

		setValues(&qsubParam, tok, value);


#ifdef NQSMOCK_DEBUG
		puts(tok);
		puts(value);
#endif
		tok = end;
	}

	qsubParam.scriptname = strdup(argv[4]);

	return qsubParam;

}

void checkQsubParam(QsubParam *qsubParam, SshAccount *sshAccount, SshServer *sshServer) {
	char *pattern = "Se ha producido un error al intentar enviar el trabajo:\n----\n\nEl valor maximo de %s que se puede solicitar es %ld\n\n----\n\n";
	char *str;

	if (qsubParam->numProc > sshServer->maxProc) {
		char *param = "num_proc";
		if ((str = malloc(sizeof(char) * (strlen(pattern) + strlen(param) + 32) )) == NULL) {
			exit_msg("checkQsubParam", "cannot allocate memory");
		}
		sprintf(str, pattern, param, (long) sshServer->maxProc);
		printf("%s\n", str);
		exit(EXIT_FAILURE);
	}

	if (qsubParam->time > sshServer->maxTime) {
		char *param = "s_rt";
		if ((str = malloc(sizeof(char) * (strlen(pattern) + strlen(param) + 32) )) == NULL) {
			exit_msg("checkQsubParam", "cannot allocate memory");
		}
		sprintf(str, pattern, param, (long) sshServer->maxProc);
		printf("%s\n", str);
		exit(EXIT_FAILURE);
	}

	if (qsubParam->memory > sshServer->maxMem) {
		char *param = "s_vmem";
		if ((str = malloc(sizeof(char) * (strlen(pattern) + strlen(param) + 32) )) == NULL) {
			exit_msg("checkQsubParam", "cannot allocate memory");
		}
		sprintf(str, pattern, param, (long) sshServer->maxMem);
		printf("%s\n", str);
		exit(EXIT_FAILURE);
	}

	if (sshAccount->numJob >= sshServer->maxJob) {
		printf("Unable to run job: job rejected: Only %ld jobs are allowed per user (current job count: %d)ºn.Exiting.\n", sshServer->maxJob, sshAccount->numJob);
		exit(EXIT_FAILURE);
	}

}

void launchJob(QsubParam *qsubParam, sqlite3 *db, SshAccount *sshAccount) {
	Job job;
	job.status = 'w';
	job.numProc = qsubParam->numProc;
	job.memory = qsubParam->memory;
	job.scriptname = qsubParam->scriptname;
	job.launchBy = sshAccount->id;

	insertJob(db, &job);
	sshAccountAddNumJob(db, sshAccount->id);

	printf("Your job %d (\"%s\") has been submitted\n", job.jobId, job.scriptname);
}

int main(int argc, char *argv[]) {
	QsubParam qsubParam = checkArgv(argc, argv);

	sqlite3 *db = openDatasource(getUrlDatasource());

	SshAccount *sshAccount = getFirstSshAccountByUsername(db, getCurrentUser());
	if (sshAccount == NULL) {
		printf("Don't exist user %s on database\n", getCurrentUser());
		closeDatasource(db);
		exit_msg("main", "username invalid");
	}

	SshServer *sshServer = getOneSshServerById(db, sshAccount->belong);

	checkQsubParam(&qsubParam, sshAccount, sshServer);

	launchJob(&qsubParam, db, sshAccount);

	closeDatasource(db);
}
