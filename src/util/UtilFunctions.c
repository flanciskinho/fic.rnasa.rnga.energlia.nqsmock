#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>

#include <ctype.h>

#include "UtilFunctions.h"
#include "UtilMacros.h"

int callback(void *NotUsed, int argc, char **argv, char **azColName) {
	int i;

	for(i=0; i<argc; i++)
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");

	printf("\n");
	return 0;
}

bool exit_msg(char *where, const char *msg) {
	printf("(%s) %s\n", where, msg);
	fflush(stdout);
    
	exit(EXIT_FAILURE);

	return false;
}

short getRandomShort(short min, short max) {
	if (min > max)
		exit_msg("getRandomShort","invalid range");

	if (min == max)
		return min;

	short tmp;
	if (min >= 0) {
		tmp = max - min;
		return (short) (min + (random() % tmp));
	}

	if (max <= 0) {
		tmp = ABS(min - max);
		return (short) max - (random() % tmp);
	}

	if (random() % 2 == 0) {
		return (short) (random() % max);
	} else {
		return (short) -1*(random() % min);
	}
}

float getRandomFloat(float min, float max) {
	if (min > max)
		exit_msg("getRandomFloat","invalid range");
	if (min == max)
		return min;

	float tmp, dec;

	dec = ((float) random())/((float) RAND_MAX);

	if (min >= 0.0f) {
		tmp = max - min;
		if (((long) tmp) != 0)
			tmp = (float) (min + (random() % ((long) tmp)));
		else
			tmp = 0.0f;
		return (tmp + dec > max)? max: tmp + dec;
	}

	if (max <= 0.0f) {
		tmp = ABS(min - max);
		if (((long) tmp) != 0)
			tmp = (float) max - (random() % ((long) tmp));
		else
			tmp = 0.0f;
		return (tmp - dec < min)? min: tmp - dec;
	}

	if (random() % 2 == 0) {
		if (((long) max) != 0)
			tmp = (float) (random() % ((long) max));
		else
			tmp = 0.0f;
		return (tmp + dec > max)? max: tmp + dec;
	} else {
		if (((long) min) != 0)
			tmp = (float) -1.0f*(random() % ((long) min));
		else
			tmp = 0.0f;
		return (tmp - dec < min)? min: tmp - dec;
	}

}

bool isFloat(char *str) {
	int i;
	
	for (i = 0; i < strlen(str); i++)
		if (isdigit(str[i]) == 0 &&  str[i]!='-' && str[i]!='.')
			return false;
	
	return true;
}

bool isPositiveFloat(char *str){
	size_t cnt;

	for (cnt = 0; cnt < strlen(str); cnt++)
		if (isdigit(str[cnt]) == 0 && str[cnt] != '.')
			return false;

	return true;
}
bool isPositiveInt(char *str){
	size_t cnt;

	for (cnt = 0; cnt < strlen(str); cnt++)
		if (isdigit(str[cnt]) == 0)
			return false;

	return true;
}

size_t findMaxValue(float *array, size_t size){
	size_t cnt, tmp;

	tmp = 0;
	for (cnt = 1; cnt < size; cnt++) {
		if (array[cnt] > array[tmp])
			tmp = cnt;
	}

	return tmp;
}

size_t findMinValue(float *array, size_t size){
	size_t cnt, tmp;

	tmp = 0;
	for (cnt = 1; cnt < size; cnt++) {
		if (array[cnt] < array[tmp])
			tmp = cnt;
	}

	return tmp;
}

bool ishigherf(float value, float cmp) {
    return (value > cmp);
}

bool islowerf (float value, float cmp) {
    return (value < cmp);
}

char *strPlus(char *s1, char *s2){
	char *tmp = malloc(sizeof(char) * (strlen(s1) + strlen(s2) + 1) );
	if (tmp == NULL)
		exit_msg("strPlus", "cannot allocate memory");

	tmp[0] = '\0';
	strcpy(tmp, s1);
	strcat(tmp, s2);

	return tmp;
}