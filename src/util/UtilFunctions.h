#ifndef RNGA_UtilFunctions_h
#define RNGA_UtilFunctions_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int callback(void *NotUsed, int argc, char **argv, char **azColName);

bool exit_msg(char *where,const char *msg);

short getRandomShort(short min, short max);

float getRandomFloat(float min, float max);

bool isFloat(char *str);

bool isPositiveFloat(char *str);

bool isPositiveInt(char *str);

size_t findMaxValue(float *array, size_t size);

size_t findMinValue(float *array, size_t size);

bool ishigherf(float value, float cmp);

bool islowerf (float value, float cmp);

char *strPlus(char *s1, char *s2);


#endif
