#ifndef RNGA_UtilMacros_h
#define RNGA_UtilMacros_h

#define ABS(x) ((x < 0) ? (-1*(x)) :(x))

#define MIN(x,y) ((x < y)? x: y)
#define MAX(x,y) ((x > y)? x: y)

#define NORMALIZE(x,y) (MAX(MIN(x, y),-x))

#define INDEX(i,j,ncols) ((i)*(ncols)+(j))

#endif
